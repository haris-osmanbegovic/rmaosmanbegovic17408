package ba.unsa.etf.rma.haris.ho17408;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Locale;

/**
 * Created by Haris on 29-Mar-17.
 */

public class ZanrAdapter extends ArrayAdapter<Zanr> {
    private ArrayList<Zanr> lista;
    public ZanrAdapter(Context context, int resource, ArrayList<Zanr> lista){
        super(context, resource, lista);
        this.lista = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View red;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            red = inflater.inflate(R.layout.pojedinacni_zanr, parent, false);
        }
        else{
            red = convertView;
        }

        ImageView slika = (ImageView) red.findViewById(R.id.slika);
        TextView naziv = (TextView) red.findViewById(R.id.naziv);

        Zanr temp = lista.get(position);
        slika.setImageResource(temp.getSlika());
        naziv.setText(temp.getNaziv());

        return red;
    }
}
