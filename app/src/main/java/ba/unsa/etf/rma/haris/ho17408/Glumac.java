package ba.unsa.etf.rma.haris.ho17408;

import android.media.Image;
import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

public class Glumac extends SugarRecord implements Parcelable {

    //region VARIABLES

    private String naziv;
    private int godinaRodjenja;
    private int godinaSmrti;
    private String slika;
    private double rejting;
    private String mjestoRodjenja;
    private boolean muskarac;
    private String IMDB;
    private String biografija;
    private int sifra;

    //endregion

    //region CONSTRUCTORS

    public Glumac(){}

    //konstruktor za slucaj ako je glumac i dalje ziv, za godinu smrti se uzima default value 0
    public Glumac(String naziv, boolean muskarac, int godinaRodjenja, String mjestoRodjenja, double rejting, String slika, String IMDB, String biografija, int sifra){
        setMuskarac(muskarac);
        setNaziv(naziv);
        setGodinaRodjenja(godinaRodjenja);
        setMjestoRodjenja(mjestoRodjenja);
        setRejting(rejting);
        setSlika(slika);
        setIMDB(IMDB);
        setBiografija(biografija);
        setSifra(sifra);
    }

    //konstruktor v2
    public Glumac(String naziv, boolean muskarac, int godinaRodjenja, String mjestoRodjenja, double rejting, String slika, String IMDB, String biografija, int godinaSmrti, int sifra){
        setMuskarac(muskarac);
        setNaziv(naziv);
        setGodinaRodjenja(godinaRodjenja);
        setMjestoRodjenja(mjestoRodjenja);
        setRejting(rejting);
        setSlika(slika);
        setIMDB(IMDB);
        setBiografija(biografija);
        setGodinaSmrti(godinaSmrti);
        setSifra(sifra);
    }

    //endregion

    //region GET/SET

    public int getGodinaSmrti() {
        return godinaSmrti;
    }

    public String getBiografija() {
        return biografija;
    }

    public String getIMDB() {
        return IMDB;
    }

    public boolean isMuskarac(){
        return muskarac;
    }

    public String getNaziv() {
        return naziv;
    }

    public String getSlika() {
        return slika;
    }

    public int getGodinaRodjenja() {
        return godinaRodjenja;
    }

    public String getMjestoRodjenja() {
        return mjestoRodjenja;
    }

    public double getRejting() {
        return rejting;
    }

    public int getSifra(){ return sifra; }

    public void setSifra(int sifra) { this.sifra = sifra; }

    public void setBiografija(String biografija) {
        this.biografija = biografija;
    }

    public void setGodinaSmrti(int godinaSmrti) {
        this.godinaSmrti = godinaSmrti;
    }

    public void setIMDB(String IMDB) {
        this.IMDB = IMDB;
    }

    public void setMuskarac(boolean muskarac){
        this.muskarac = muskarac;
    }

    public void setGodinaRodjenja(int godinaRodjenja) {
        this.godinaRodjenja = godinaRodjenja;
    }

    public void setMjestoRodjenja(String mjestoRodjenja) {
        this.mjestoRodjenja = mjestoRodjenja;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public void setRejting(double rejting) {
        this.rejting = rejting;
    }

    //endregion

    //region PARCELABLE

    protected Glumac(Parcel in) {
        naziv = in.readString();
        godinaRodjenja = in.readInt();
        godinaSmrti = in.readInt();
        slika = in.readString();
        rejting = in.readDouble();
        mjestoRodjenja = in.readString();
        muskarac = in.readByte() != 0x00;
        IMDB = in.readString();
        biografija = in.readString();
        sifra = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeInt(godinaRodjenja);
        dest.writeInt(godinaSmrti);
        dest.writeString(slika);
        dest.writeDouble(rejting);
        dest.writeString(mjestoRodjenja);
        dest.writeByte((byte) (muskarac ? 0x01 : 0x00));
        dest.writeString(IMDB);
        dest.writeString(biografija);
        dest.writeInt(sifra);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Glumac> CREATOR = new Parcelable.Creator<Glumac>() {
        @Override
        public Glumac createFromParcel(Parcel in) {
            return new Glumac(in);
        }

        @Override
        public Glumac[] newArray(int size) {
            return new Glumac[size];
        }
    };


    //endregion
}