package ba.unsa.etf.rma.haris.ho17408;

import android.os.Bundle;
import android.os.Handler;
import android.os.ResultReceiver;

public class MojResultReceiver extends ResultReceiver {
    private Receiver resiver;

    public MojResultReceiver(Handler handler){
        super(handler);
    }

    public void setReceiver(Receiver receiver){
        this.resiver = receiver;
    }

    public interface Receiver{
        public void onReceiveResult(int resultCode, Bundle ResultData);
    }

    @Override
    protected void onReceiveResult(int resultCode, Bundle resultData) {
        if(resiver != null){
            resiver.onReceiveResult(resultCode, resultData);
        }
    }
}
