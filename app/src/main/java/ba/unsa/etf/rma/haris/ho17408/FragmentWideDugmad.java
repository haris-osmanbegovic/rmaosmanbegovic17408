package ba.unsa.etf.rma.haris.ho17408;

import android.app.Activity;
import android.app.Fragment;
import android.media.Image;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

public class FragmentWideDugmad extends Fragment {
    promjenaFragmenata roditelj;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.wide_fragment_dugmad, container, false);

        Button ostalo = (Button)v.findViewById(R.id.wide_frag_butt_ostalo);
        ostalo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    roditelj = (promjenaFragmenata) getActivity();
                }
                catch (Exception e){
                    throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
                }

                roditelj.promijeniFragmenteWide("ostalo");
            }
        });

        Button glumci = (Button)v.findViewById(R.id.wide_frag_butt_glumci);
        glumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    roditelj = (promjenaFragmenata) getActivity();
                }
                catch (Exception e){
                    throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
                }

                roditelj.promijeniFragmenteWide("glumci");
            }
        });

        Button film = (Button)v.findViewById(R.id.wide_frag_butt_film);
        film.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    roditelj = (promjenaFragmenata) getActivity();
                }
                catch (Exception e){
                    throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
                }

                roditelj.promijeniFragmenteWide("filmovi");
            }
        });

        ImageView zastava = (ImageView)v.findViewById(R.id.zastava);
        zastava.setImageResource(R.drawable.zastava);



        return v;
    }

    public interface promjenaFragmenata{
        public void promijeniFragmenteWide(String kod);
    }
}
