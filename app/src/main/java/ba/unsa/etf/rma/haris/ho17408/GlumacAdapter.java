package ba.unsa.etf.rma.haris.ho17408;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Locale;

public class GlumacAdapter extends ArrayAdapter<Glumac> {
    private ArrayList<Glumac> lista;
    public GlumacAdapter(Context context, int resource, ArrayList<Glumac> lista){
        super(context, resource, lista);
        this.lista = lista;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View red;
        if(convertView == null) {
            LayoutInflater inflater = LayoutInflater.from(getContext());
            red = inflater.inflate(R.layout.pojedinacni_glumac, parent, false);
        }
        else{
            red = convertView;
        }

        ImageView slika = (ImageView) red.findViewById(R.id.slika);
        TextView naziv = (TextView) red.findViewById(R.id.naziv);
        TextView rejting = (TextView) red.findViewById(R.id.rejting);
        TextView mjestorodjenja = (TextView) red.findViewById(R.id.mjestorodjenja);
        TextView godinarodjenja = (TextView) red.findViewById(R.id.godinarodjenja);

        Glumac temp = lista.get(position);
        Picasso.with(getContext()).load("https://image.tmdb.org/t/p/w500" + temp.getSlika()).into(slika);
        naziv.setText(temp.getNaziv());
        rejting.setText(String.format(Locale.US, "%3.2f", temp.getRejting()));
        if(!temp.getMjestoRodjenja().equals("null"))
            if(temp.getMjestoRodjenja().length() > 30)
                mjestorodjenja.setText(temp.getMjestoRodjenja().substring(0, 31));
            else
                mjestorodjenja.setText(temp.getMjestoRodjenja());
        else
            mjestorodjenja.setText("");

        if(temp.getGodinaSmrti() != 0)
            godinarodjenja.setText(Integer.toString(temp.getGodinaRodjenja()) + "-" + Integer.toString(temp.getGodinaSmrti()));
        else if (temp.getGodinaRodjenja() != 0)
            godinarodjenja.setText(Integer.toString(temp.getGodinaRodjenja()));
        else
            godinarodjenja.setText("");

        return red;
    }
}
