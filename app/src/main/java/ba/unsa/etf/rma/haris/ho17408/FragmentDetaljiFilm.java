package ba.unsa.etf.rma.haris.ho17408;

import android.app.Fragment;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import java.util.Calendar;

public class FragmentDetaljiFilm extends Fragment {
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View za_vratiti = inflater.inflate(R.layout.pojedinacni_film, container, false);

        if(getArguments() != null){
            final Bundle argumenti = getArguments();
            TextView naziv = (TextView) za_vratiti.findViewById(R.id.nazivFilma);
            naziv.setText(argumenti.getString("naziv"));

            final EditText note = (EditText) za_vratiti.findViewById(R.id.komentar);

            Button zapamti = (Button) za_vratiti.findViewById(R.id.zapamti);
            zapamti.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Calendar beginTime = Calendar.getInstance();
                    DatePicker odabirDatuma = (DatePicker) za_vratiti.findViewById(R.id.izborDatuma);
                    beginTime.set(odabirDatuma.getYear(), odabirDatuma.getMonth(), odabirDatuma.getDayOfMonth());
                    Intent intent = new Intent(Intent.ACTION_INSERT)
                            .setData(CalendarContract.Events.CONTENT_URI)
                            .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                            .putExtra(CalendarContract.Events.TITLE, argumenti.getString("naziv"))
                            .putExtra(CalendarContract.Events.DESCRIPTION, note.getText().toString());
                    startActivity(intent);
                }
            });
        }
        return za_vratiti;
    }
}
