package ba.unsa.etf.rma.haris.ho17408;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Ova klasa sluzi za pohranjivanje vrijednosti koje ce se zasad koristiti u aplikaciji
 */

final public class kontejner {
    public static ArrayList<Glumac> glumci = new ArrayList<>();
    public static ArrayList<Reziser> reziseri = new ArrayList<>();
    public static ArrayList<Zanr> zanrovi = new ArrayList<>();
    public static ArrayList<String> filmovi = new ArrayList<>();
    private static boolean ucitano = false;

    public static void dodajZanrSaProvjerom(String zanrZaUbaciti){
        for(Zanr z : zanrovi)
            if(z.getNaziv().equals(zanrZaUbaciti))
                return;

        zanrovi.add(new Zanr(zanrZaUbaciti, glumci.get(0)));
    }

    public static void dodajReziseraSaProvjerom(String reziserZaUbaciti){
        for(Reziser s : reziseri)
            if(s.getNaziv().equals(reziserZaUbaciti))
                return;

        reziseri.add(new Reziser(reziserZaUbaciti, glumci.get(0)));
    }

    public static void ucitajPodatke(){
        /*
        if(ucitano)
            return;

        Glumac admir = new Glumac("Admir Glamocak", true, 1962, "Sarajevo", 3.55, R.drawable.admir_glamocak, "http://www.imdb.com/name/nm0321708/", "test");
        Glumac elizabeth = new Glumac("Elisabeth Taylor", false, 1932, "London", 4.055, R.drawable.elizabeth_taylor, "http://www.imdb.com/name/nm0000072/", "test", 2011);
        glumci.add(admir);
        glumci.add(elizabeth);

        reziseri.add("Christopher Nolan");
        reziseri.add("Quentin Tarantino");

        zanrovi.add(new Zanr("Adventure", R.drawable.adventure));
        zanrovi.add(new Zanr("Animation", R.drawable.animation));
        zanrovi.add(new Zanr("Crime", R.drawable.crime));

        ucitano = true;
        */
    }
}
