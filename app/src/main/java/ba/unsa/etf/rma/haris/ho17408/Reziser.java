package ba.unsa.etf.rma.haris.ho17408;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarRecord;

/**
 * Created by Haris on 29-Mar-17.
 */

public class Reziser extends SugarRecord implements Parcelable {
    private String naziv;
    private Glumac glumac;

    public Reziser(){}

    public Reziser(String naziv, Glumac glumac){
        setNaziv(naziv);
        setGlumac(glumac);
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public void setGlumac(Glumac glumac){ this.glumac = glumac; }

    public Glumac getGlumac() { return this.glumac; }

    protected Reziser(Parcel in) {
        naziv = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Reziser> CREATOR = new Parcelable.Creator<Reziser>() {
        @Override
        public Reziser createFromParcel(Parcel in) {
            return new Reziser(in);
        }

        @Override
        public Reziser[] newArray(int size) {
            return new Reziser[size];
        }
    };
}