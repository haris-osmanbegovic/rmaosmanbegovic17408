package ba.unsa.etf.rma.haris.ho17408;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.Image;
import android.os.Bundle;
import android.os.ResultReceiver;
import android.renderscript.ScriptGroup;
import android.support.annotation.Nullable;
import android.widget.Toast;

import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import static java.security.AccessController.getContext;

public class PretragaTheMovieDB extends IntentService {

    public static int STATUS_RUNNING = 0;
    public static int STATUS_FINISHED = 1;
    public static int STATUS_ERROR = 2;

    public PretragaTheMovieDB() {
        super(null);
    }

    @Override
    protected void onHandleIntent(@Nullable Intent intent) {
        String komanda = intent.getStringExtra("komanda");
        String query = intent.getStringExtra("query");
        final ResultReceiver receiver = intent.getParcelableExtra("res");
        Bundle rezultat = new Bundle();

        if (query.contains("actor:")) {
            kontejner.glumci.clear();
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            query = query.replace("actor:", "");
            List<Glumac> tempG = Glumac.find(Glumac.class, "naziv = ?", query);
            for (Glumac g : tempG)
                kontejner.glumci.add(g);
            rezultat.putString("kod", "gotovo");
            receiver.send(STATUS_FINISHED, rezultat);
        } else if (query.contains("director:")) {
            kontejner.glumci.clear();
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            query = query.replace("director:", "");
            List<Reziser> tempR = Reziser.find(Reziser.class, "naziv = ?", query);
            if (tempR.size() != 0)
                kontejner.glumci.add(tempR.get(0).getGlumac());

            rezultat.putString("kod", "gotovo");
            receiver.send(STATUS_FINISHED, rezultat);
        } else if (komanda.equals("dajglumce")) {
            kontejner.glumci.clear();
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            try {
                String fullQuery = "https://api.themoviedb.org/3/search/person?api_key=8c0673376c85cc405aec5e1b7968aa2b&query=" + query;
                URL url = new URL(fullQuery);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String odgovor = convertStreamToString(in);
                JSONObject jo = new JSONObject(odgovor);
                JSONArray glumci = jo.getJSONArray("results");

                for (int i = 0; i < glumci.length() && i < 7; i++) {
                    JSONObject tempGlumac = glumci.getJSONObject(i);
                    int sifraGlumca = tempGlumac.getInt("id");
                    Boolean glumacJe = false;

                    JSONArray filmovi = glumci.getJSONObject(i).getJSONArray("known_for");
                    if (filmovi != null) {
                        for (int j = 0; j < filmovi.length(); j++) {
                            int idFilma = filmovi.getJSONObject(j).getInt("id");

                            //imamo ID filma, trazimo ovu osobu u "cast" dijelu filma, ukoliko bude glumac
                            //barem u jednom filmu, stavljamo ga u listu glumaca
                            URL urlFilm;
                            String fullQueryFilm;
                            HttpURLConnection urlConnectionFilm = null;
                            InputStream inFilm = null;
                            try {
                                fullQueryFilm = "http://api.themoviedb.org/3/movie/" + idFilma + "?api_key=8c0673376c85cc405aec5e1b7968aa2b&append_to_response=credits";
                                urlFilm = new URL(fullQueryFilm);
                                urlConnectionFilm = (HttpURLConnection) urlFilm.openConnection();
                                inFilm = new BufferedInputStream(urlConnectionFilm.getInputStream());
                            } catch (Exception e) {
                                //ne radi se o filmu nego o tv showu
                                fullQueryFilm = "http://api.themoviedb.org/3/tv/" + idFilma + "?api_key=8c0673376c85cc405aec5e1b7968aa2b&append_to_response=credits";
                                urlFilm = new URL(fullQueryFilm);
                                urlConnectionFilm = (HttpURLConnection) urlFilm.openConnection();
                                inFilm = new BufferedInputStream(urlConnectionFilm.getInputStream());
                            } finally {
                                String odgovorFilm = convertStreamToString(inFilm);
                                JSONObject film = new JSONObject(odgovorFilm);
                                JSONObject credits = film.getJSONObject("credits");
                                JSONArray cast = credits.getJSONArray("cast");
                                for (int k = 0; k < cast.length(); k++) {
                                    if (sifraGlumca == cast.getJSONObject(k).getInt("id")) {
                                        glumacJe = true;
                                        j = filmovi.length();
                                        break;
                                    }
                                }
                            }

                        }
                    }


                    if (glumacJe) {
                        //imamo sifru glumca, sada trazimo i ostale podatke kako bi mogli da napravimo objekat Glumac
                        String fullQueryGlumac = "http://api.themoviedb.org/3/person/" + sifraGlumca + "?api_key=8c0673376c85cc405aec5e1b7968aa2b";
                        URL urlGlumac = new URL(fullQueryGlumac);
                        HttpURLConnection urlConnectionGlumac = (HttpURLConnection) urlGlumac.openConnection();
                        InputStream inGlumac = new BufferedInputStream(urlConnectionGlumac.getInputStream());
                        String odgovorGlumac = convertStreamToString(inGlumac);
                        JSONObject glumac = new JSONObject(odgovorGlumac);

                        //dobili smo objekat koji trazimo, izvlacimo potrebne podatke
                        String naziv = glumac.getString("name");
                        Boolean muskarac;
                        int spol = glumac.getInt("gender");
                        if (spol == 1 || spol == 0)
                            muskarac = false;
                        else
                            muskarac = true;
                        String datumRodjenja = glumac.getString("birthday");
                        int godinaRodjenja = 0;
                        if (!datumRodjenja.equals("null") && !datumRodjenja.equals(""))
                            godinaRodjenja = Integer.parseInt(datumRodjenja.substring(0, 4));
                        String tempMjestoRodjenja = glumac.getString("place_of_birth");
                        String mjestoRodjenja = tempMjestoRodjenja;
                        if (!tempMjestoRodjenja.equals("null") && !tempMjestoRodjenja.equals(""))
                            mjestoRodjenja = tempMjestoRodjenja.split(",")[0];
                        double rejting = glumac.getDouble("popularity");
                        String slika = glumac.getString("profile_path");
                        String imdb = glumac.getString("imdb_id");
                        String biografija = glumac.getString("biography");

                        String datumSmrti = glumac.getString("deathday");
                        if (datumSmrti.equals("") || datumSmrti.equals("null"))
                            kontejner.glumci.add(new Glumac(naziv, muskarac, godinaRodjenja, mjestoRodjenja, rejting, slika, imdb, biografija, sifraGlumca));
                        else {
                            int godinaSmrti = Integer.parseInt(datumSmrti.substring(0, 4));
                            kontejner.glumci.add(new Glumac(naziv, muskarac, godinaRodjenja, mjestoRodjenja, rejting, slika, imdb, biografija, godinaSmrti, sifraGlumca));
                        }
                    }
                }

                rezultat.putString("kod", "gotovo");
                receiver.send(STATUS_FINISHED, rezultat);
            } catch (Exception e) {
                rezultat.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, rezultat);
            }
        } else if (komanda.equals("dajostalo")) {
            List<Glumac> temp = Glumac.find(Glumac.class, "sifra = ?", query);
            if (temp.size() != 0) {
                kontejner.reziseri = (ArrayList) Reziser.find(Reziser.class, "glumac = ?", Long.toString(temp.get(0).getId()));
                kontejner.zanrovi = (ArrayList) Zanr.find(Zanr.class, "glumac = ?", Long.toString(temp.get(0).getId()));
                rezultat.putString("kod", "gotovo");
                receiver.send(STATUS_FINISHED, rezultat);
                return;
            }
            kontejner.reziseri.clear();
            kontejner.zanrovi.clear();
            try {
                String fullQuery = "http://api.themoviedb.org/3/discover/movie?api_key=8c0673376c85cc405aec5e1b7968aa2b&sort_by=release_date.desc&with_cast=" + query;
                URL url = new URL(fullQuery);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String odgovor = convertStreamToString(in);
                JSONObject jo = new JSONObject(odgovor);
                JSONArray filmovi = jo.getJSONArray("results");

                for (int i = 0; i < filmovi.length() && i < 7; i++) {
                    JSONObject tempFilm = filmovi.getJSONObject(i);
                    int id = tempFilm.getInt("id");

                    //imamo id filma, trazimo detalje i produkciju
                    String fullQueryFilm = "http://api.themoviedb.org/3/movie/" + id + "?api_key=8c0673376c85cc405aec5e1b7968aa2b&append_to_response=credits";
                    URL urlFilm = new URL(fullQueryFilm);
                    HttpURLConnection urlConnectionFilm = (HttpURLConnection) urlFilm.openConnection();
                    InputStream inFilm = new BufferedInputStream(urlConnectionFilm.getInputStream());
                    String odgovorFilm = convertStreamToString(inFilm);
                    JSONObject film = new JSONObject(odgovorFilm);

                    //imamo film, sada izvlacimo potrebne podatke
                    JSONArray genres = film.getJSONArray("genres");
                    //zbog zahtjeva da 1 film = 1 zanr, idemo samo indeks 0
                    if (genres.length() != 0)
                        kontejner.dodajZanrSaProvjerom(genres.getJSONObject(0).getString("name"));

                    JSONObject credits = film.getJSONObject("credits");
                    JSONArray crew = credits.getJSONArray("crew");
                    kontejner.dodajReziseraSaProvjerom(crew.getJSONObject(0).getString("name"));
                }
                rezultat.putString("kod", "gotovo");
                receiver.send(STATUS_FINISHED, rezultat);
            } catch (Exception e) {
                rezultat.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, rezultat);
            }
        }

        else if(komanda.equals("dajfilmove")){
            receiver.send(STATUS_RUNNING, Bundle.EMPTY);
            kontejner.filmovi.clear();

            try {
                String fullQuery = "http://api.themoviedb.org/3/search/movie?api_key=8c0673376c85cc405aec5e1b7968aa2b&query=" + query;
                URL url = new URL(fullQuery);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
                InputStream in = new BufferedInputStream(urlConnection.getInputStream());
                String odgovor = convertStreamToString(in);
                JSONObject jo = new JSONObject(odgovor);
                JSONArray filmovi = jo.getJSONArray("results");

                for(int i = 0; i < 10 && i < filmovi.length(); i++){
                    //uzimamo prvih 10 filmova, vise nam ne treba
                    JSONObject film = filmovi.getJSONObject(i);
                    kontejner.filmovi.add(film.getString("title"));
                }

                rezultat.putString("kod", "gotovo");
                receiver.send(STATUS_FINISHED, rezultat);
            }
            catch(Exception e){
                rezultat.putString(Intent.EXTRA_TEXT, e.toString());
                receiver.send(STATUS_ERROR, rezultat);
            }
        }

    }

    private String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (Exception e) {

        } finally {
            try {
                is.close();
            } catch (IOException e) {

            }
        }
        return sb.toString();
    }
}
