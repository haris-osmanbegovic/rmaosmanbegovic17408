package ba.unsa.etf.rma.haris.ho17408;

import android.os.Parcel;
import android.os.Parcelable;

import com.orm.SugarApp;
import com.orm.SugarRecord;

import static java.security.AccessController.getContext;

public class Zanr extends SugarRecord implements Parcelable {
    private String naziv;
    private int slika;
    private Glumac glumac;

    public Zanr(){}

    public Zanr(String naziv, Glumac g){
        setNaziv(naziv);
        setGlumac(g);
        switch(naziv){
            case "Adventure":
                setSlika(R.drawable.adventure);
                break;
            case "Animation":
                setSlika(R.drawable.animation);
                break;
            case "Crime":
                setSlika(R.drawable.crime);
                break;
            case "Horror":
                setSlika(R.drawable.horror);
                break;
            case "Comedy":
                setSlika(R.drawable.comedy);
                break;
            case "Documentary":
                setSlika(R.drawable.documentary);
                break;
            case "Action":
                setSlika(R.drawable.action);
                break;
            case "Romance":
                setSlika(R.drawable.romance);
                break;
            case "Sport":
                setSlika(R.drawable.sport);
                break;
            default:
                setSlika(R.drawable.others);
                break;
        }
    }

    public Zanr(String naziv, int slika, Glumac glumac){
        setNaziv(naziv);
        setSlika(slika);
        setGlumac(glumac);
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public int getSlika() {
        return slika;
    }

    public void setSlika(int slika) {
        this.slika = slika;
    }

    public void setGlumac(Glumac glumac){
        this.glumac = glumac;
    }

    protected Zanr(Parcel in) {
        naziv = in.readString();
        slika = in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeInt(slika);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Zanr> CREATOR = new Parcelable.Creator<Zanr>() {
        @Override
        public Zanr createFromParcel(Parcel in) {
            return new Zanr(in);
        }

        @Override
        public Zanr[] newArray(int size) {
            return new Zanr[size];
        }
    };
}