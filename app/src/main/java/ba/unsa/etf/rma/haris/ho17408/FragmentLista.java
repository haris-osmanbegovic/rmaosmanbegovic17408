package ba.unsa.etf.rma.haris.ho17408;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcel;
import android.support.annotation.Nullable;
import android.support.v4.widget.ListViewAutoScrollHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class FragmentLista extends Fragment implements MojResultReceiver.Receiver {
    static Context myContext;
    MojResultReceiver resiver;
    PrikazivanjeDetalja aktivnost;
    View za_vratiti;
    Bundle parcela;
    String kod;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        myContext = getContext();
        za_vratiti = inflater.inflate(R.layout.fragment_lista, container, false);

        try{
            aktivnost = (PrikazivanjeDetalja) getActivity();
        }
        catch(ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
        }

        if(getArguments() != null) {
            parcela = getArguments();
            kod = parcela.getString("kod");

            ListView lista = (ListView) za_vratiti.findViewById(R.id.lista);

            switch (kod) {
                case "glumci":
                    GlumacAdapter adapter = new GlumacAdapter(za_vratiti.getContext(), R.layout.pojedinacni_glumac, kontejner.glumci);
                    lista.setAdapter(adapter);
                    lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                        @Override
                        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                            resiver = new MojResultReceiver(new Handler());
                            resiver.setReceiver(FragmentLista.this);
                            Intent intent = new Intent(Intent.ACTION_SYNC, null, getContext(), PretragaTheMovieDB.class);
                            intent.putExtra("res", resiver);
                            intent.putExtra("komanda", "dajostalo");
                            intent.putExtra("query", Integer.toString(kontejner.glumci.get(position).getSifra()));
                            getActivity().startService(intent);
                            aktivnost.prikaziDetalje(position);
                        }
                    });
                    break;
                case "zanrovi":
                    FrameLayout desni = (FrameLayout) getActivity().findViewById(R.id.frame_wide_desno);
                    if(desni != null){
                        EditText tekst = (EditText) za_vratiti.findViewById(R.id.tekst_pretraga);
                        Button pretraga = (Button) za_vratiti.findViewById(R.id.pretraga);
                        tekst.setVisibility(View.INVISIBLE);
                        pretraga.setVisibility(View.INVISIBLE);
                    }
                    ZanrAdapter adapter2 = new ZanrAdapter(za_vratiti.getContext(), R.layout.pojedinacni_zanr, kontejner.zanrovi);
                    lista.setAdapter(adapter2);
                    break;
                case "reziseri":
                    ArrayList<String> rez = new ArrayList<String>();
                    for(Reziser r : kontejner.reziseri)
                        rez.add(r.getNaziv());

                    ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(za_vratiti.getContext(), android.R.layout.simple_list_item_1, rez);
                    lista.setAdapter(adapter3);
                    break;
            }

            Button pretraga = (Button) za_vratiti.findViewById(R.id.pretraga);
            pretraga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    resiver = new MojResultReceiver(new Handler());
                    resiver.setReceiver(FragmentLista.this);
                    EditText tekst = (EditText) za_vratiti.findViewById(R.id.tekst_pretraga);
                    Intent intent = new Intent(Intent.ACTION_SYNC, null, getContext(), PretragaTheMovieDB.class);
                    intent.putExtra("res", resiver);
                    intent.putExtra("komanda", "dajglumce");
                    intent.putExtra("query", tekst.getText().toString());
                    getActivity().startService(intent);
                }
            });
        }


        return za_vratiti;
    }

    public interface PrikazivanjeDetalja{
        public void prikaziDetalje(int pozicija);
    }

    public Context dajKontekst(){
        return myContext;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle ResultData) {
        switch(resultCode){
            case 0:
                Toast.makeText(dajKontekst(), "Dohvacanje rezultata u toku", Toast.LENGTH_LONG).show();
                break;
            case 1:
                ListView lista = (ListView) za_vratiti.findViewById(R.id.lista);
                ArrayAdapter adapter = (ArrayAdapter) lista.getAdapter();
                adapter.notifyDataSetChanged();

                lista.requestLayout();

                /*if(lista.getAdapter() == null) {
                    GlumacAdapter adapter = new GlumacAdapter(za_vratiti.getContext(), R.layout.pojedinacni_glumac, kontejner.glumci);
                    lista.setAdapter(adapter);
                }
                else if(lista.getAdapter() instanceof GlumacAdapter) {
                    GlumacAdapter ga = (GlumacAdapter) lista.getAdapter();
                    ga.notifyDataSetChanged();

                    lista.requestLayout();
                }
                */
                break;
            case 2:
                Toast.makeText(dajKontekst(), "Greska", Toast.LENGTH_LONG).show();
        }
    }
}
