package ba.unsa.etf.rma.haris.ho17408;

import android.app.Fragment;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class FragmentDetaljiGlumac extends Fragment {
    Baza aktivnost;
    Glumac temp;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        final View za_vratiti = inflater.inflate(R.layout.biografija_glumca, container, false);

        if(getArguments() != null){
            Bundle argumenti = getArguments();
            temp = argumenti.getParcelable("glumac");

            final TextView imdb = (TextView) za_vratiti.findViewById(R.id.imdb);
            final TextView biografija = (TextView) za_vratiti.findViewById(R.id.biografija);
            TextView spol = (TextView) za_vratiti.findViewById(R.id.spol);
            TextView godine = (TextView) za_vratiti.findViewById(R.id.godine);
            TextView mjestorodjenja = (TextView) za_vratiti.findViewById(R.id.mjestorodjenja);
            TextView naziv = (TextView) za_vratiti.findViewById(R.id.naziv);
            ImageView slika = (ImageView) za_vratiti.findViewById(R.id.slika);

            imdb.setText("IMDB ID: " + temp.getIMDB());
            biografija.setText(temp.getBiografija());
            if(temp.isMuskarac()) {
                spol.setText(getString(R.string.spolmusko));
                za_vratiti.setBackgroundColor(Color.CYAN);
            }
            else {
                spol.setText(getString(R.string.spolzensko));
                za_vratiti.setBackgroundColor(Color.MAGENTA);
            }

            if(temp.getGodinaSmrti() == 0)
                godine.setText(temp.getGodinaRodjenja() + "-");
            else
                godine.setText(temp.getGodinaRodjenja() + "-" + temp.getGodinaSmrti());

            mjestorodjenja.setText(temp.getMjestoRodjenja());
            naziv.setText(temp.getNaziv());
            Picasso.with(getContext()).load("https://image.tmdb.org/t/p/w500" + temp.getSlika()).into(slika);

            imdb.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String url = (String) "http://www.imdb.com/name/" + temp.getIMDB();
                    Intent otvoriWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                    if(otvoriWeb.resolveActivity(getActivity().getPackageManager()) != null)
                        startActivity(otvoriWeb);
                }
            });

            Button podijeli = (Button) za_vratiti.findViewById(R.id.podijeli);
            podijeli.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String bio = (String) biografija.getText();
                    Intent posaljiDalje = new Intent(Intent.ACTION_SEND);
                    posaljiDalje.putExtra(Intent.EXTRA_TEXT, bio);
                    posaljiDalje.setType("text/plain");
                    if(posaljiDalje.resolveActivity(getActivity().getPackageManager()) != null)
                        startActivity(posaljiDalje);
                }
            });

            try{
                aktivnost = (Baza) getActivity();
            }
            catch(ClassCastException e){
                throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
            }

            Button bookmark = (Button) za_vratiti.findViewById(R.id.bookmark);
            bookmark.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aktivnost.spasiUBazu(temp);
                }
            });

            Button obrisi = (Button) za_vratiti.findViewById(R.id.delete);
            obrisi.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    aktivnost.brisiIzBaze(temp);
                }
            });
        }

        return za_vratiti;
    }

    public interface Baza{
        public void spasiUBazu(Glumac g);
        public void brisiIzBaze(Glumac g);
    }
}


