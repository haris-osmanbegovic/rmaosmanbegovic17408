package ba.unsa.etf.rma.haris.ho17408;

import android.app.Activity;
import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

/**
 * Created by osman on 4/13/17.
 */

public class FragmentDugmad extends Fragment {
    promjenaListe aktivnost;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        View za_vratiti = inflater.inflate(R.layout.fragment_dugmadi, container, false);

        try{
            aktivnost = (promjenaListe)getActivity();
        }
        catch(ClassCastException e){
            throw new ClassCastException(getActivity().toString() + "\n Nema implementacije");
        }

        Button glumci = (Button)za_vratiti.findViewById(R.id.dugme_glumci);
        Button zanrovi = (Button)za_vratiti.findViewById(R.id.dugme_zanrovi);
        Button reziseri = (Button)za_vratiti.findViewById(R.id.dugme_reziseri);
        Button filmovi = (Button)za_vratiti.findViewById(R.id.film);

        filmovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aktivnost.promijeniNaFilmove();
            }
        });

        glumci.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aktivnost.promijeniListu("glumci");
            }
        });

        zanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aktivnost.promijeniListu("zanrovi");
            }
        });

        reziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                aktivnost.promijeniListu("reziseri");
            }
        });

        ImageView zastava = (ImageView)za_vratiti.findViewById(R.id.imageView);
        zastava.setImageResource(R.drawable.zastava);


        return za_vratiti;
    }

    public interface promjenaListe{
        public void promijeniListu(String kod);
        public void promijeniNaFilmove();
    }

}
