package ba.unsa.etf.rma.haris.ho17408;

import android.content.Intent;
import android.graphics.Color;
import android.media.Image;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import static ba.unsa.etf.rma.haris.ho17408.R.id.slika;

public class biografija_glumca extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.biografija_glumca);

        final Intent intent = getIntent();
        Glumac temp = intent.getParcelableExtra("glumac");

        final TextView imdb = (TextView) findViewById(R.id.imdb);
        final TextView biografija = (TextView) findViewById(R.id.biografija);
        TextView spol = (TextView) findViewById(R.id.spol);
        TextView godine = (TextView) findViewById(R.id.godine);
        TextView mjestorodjenja = (TextView) findViewById(R.id.mjestorodjenja);
        TextView naziv = (TextView) findViewById(R.id.naziv);
        ImageView slika = (ImageView) findViewById(R.id.slika);

        imdb.setText(temp.getIMDB());
        biografija.setText(temp.getBiografija());
        if(temp.isMuskarac()) {
            spol.setText(getString(R.string.spolmusko));
            this.findViewById(android.R.id.content).setBackgroundColor(Color.CYAN);
        }
        else {
            spol.setText(getString(R.string.spolzensko));
            this.findViewById(android.R.id.content).setBackgroundColor(Color.MAGENTA);
        }

        if(temp.getGodinaSmrti() == 0)
            godine.setText(temp.getGodinaRodjenja() + "-");
        else
            godine.setText(temp.getGodinaRodjenja() + "-" + temp.getGodinaSmrti());

        mjestorodjenja.setText(temp.getMjestoRodjenja());
        naziv.setText(temp.getNaziv());
        Picasso.with(this).load("https://image.tmdb.org/t/p/w500" + temp.getSlika()).centerInside().into(slika);

        imdb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String url = (String) imdb.getText();
                Intent otvoriWeb = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                if(otvoriWeb.resolveActivity(getPackageManager()) != null)
                    startActivity(otvoriWeb);
            }
        });

        Button podijeli = (Button) findViewById(R.id.podijeli);
        podijeli.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String bio = (String) biografija.getText();
                Intent posaljiDalje = new Intent(Intent.ACTION_SEND);
                posaljiDalje.putExtra(Intent.EXTRA_TEXT, bio);
                posaljiDalje.setType("text/plain");
                if(posaljiDalje.resolveActivity(getPackageManager()) != null)
                    startActivity(posaljiDalje);
            }
        });


    }
}
