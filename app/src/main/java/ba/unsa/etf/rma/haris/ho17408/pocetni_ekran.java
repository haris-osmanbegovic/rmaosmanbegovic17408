package ba.unsa.etf.rma.haris.ho17408;

import android.app.FragmentManager;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

import com.facebook.stetho.Stetho;

import java.util.ArrayList;
import java.util.List;

public class pocetni_ekran extends AppCompatActivity implements FragmentDugmad.promjenaListe, FragmentDetaljiGlumac.Baza, FragmentLista.PrikazivanjeDetalja, FragmentWideDugmad.promjenaFragmenata, FragmentFilm.Film {

    FrameLayout desni;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Stetho.initializeWithDefaults(this);
        setContentView(R.layout.pocetni_ekran);

        kontejner.ucitajPodatke();

        FragmentManager fm = getFragmentManager();

        desni = (FrameLayout)findViewById(R.id.frame_wide_desno);

        if(desni != null) {
            FragmentWideDugmad fwd = (FragmentWideDugmad) fm.findFragmentById(R.id.frame_wide_dugmad);
            if(fwd == null) {
                fwd = new FragmentWideDugmad();
                fm.beginTransaction().replace(R.id.frame_wide_dugmad, fwd).commit();
            }

            try {
                FragmentDetaljiGlumac fdg = (FragmentDetaljiGlumac) fm.findFragmentById(R.id.frame_wide_desno);
                if (fdg == null) {
                    Bundle argumenti = new Bundle();
                    argumenti.putParcelable("glumac", kontejner.glumci.get(0));
                    fdg = new FragmentDetaljiGlumac();
                    fdg.setArguments(argumenti);
                    fm.beginTransaction().replace(R.id.frame_wide_desno, fdg).commit();
                }
            }
            catch(Exception e){
                    FragmentDetaljiGlumac temp = new FragmentDetaljiGlumac();
                    Bundle argumenti = new Bundle();
                    if(kontejner.glumci.size() != 0) {
                        argumenti.putParcelable("glumac", kontejner.glumci.get(0));
                        temp.setArguments(argumenti);
                    }
                    fm.beginTransaction().replace(R.id.frame_wide_desno, temp).commit();
                }
        }
        else{
            FragmentDugmad fd = (FragmentDugmad)fm.findFragmentById(R.id.frame_dugmad);
            if(fd == null) {
                fd = new FragmentDugmad();
                fm.beginTransaction().replace(R.id.frame_dugmad, fd).commit();
            }
        }

        try {
            FragmentLista fl = (FragmentLista) fm.findFragmentById(R.id.frame_lista);

            if (fl == null) {
                fl = new FragmentLista();
                Bundle arguments = new Bundle();
                arguments.putString("kod", "glumci");
                fl.setArguments(arguments);
                fm.beginTransaction().replace(R.id.frame_lista, fl).commit();
            }
        }
        catch(Exception e) {
            //bili smo u fragmentu detalji, te konverzija iznad nije prosla
            FragmentLista fl = new FragmentLista();
            Bundle arguments = new Bundle();
            arguments.putString("kod", "glumci");
            fl.setArguments(arguments);
            fm.beginTransaction().replace(R.id.frame_lista, fl).commit();
        }

        //region STARI KOD
        /*

        ListView listaGlumaca = (ListView) findViewById(R.id.listaglumaca);
        GlumacAdapter adapter = new GlumacAdapter(this, R.layout.pojedinacni_glumac, kontejner.glumci);

        listaGlumaca.setAdapter(adapter);
        listaGlumaca.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(pocetni_ekran.this, biografija_glumca.class);
                intent.putExtra("glumac", kontejner.glumci.get(position));
                startActivity(intent);
            }
        });

        final Button reziseri = (Button) findViewById(R.id.reziseri);
        reziseri.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prebacivanje = new Intent(pocetni_ekran.this, reziseri.class);
                startActivity(prebacivanje);
            }
        });

        final Button zanrovi = (Button) findViewById(R.id.zanrovi);
        zanrovi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent prebacivanje = new Intent(pocetni_ekran.this, zanrovi.class);
                startActivity(prebacivanje);
            }
        });

        //Vec smo u activity-u glumci, nema potrebe da prebacujemo kada se klikne button "glumci"

        */

        //endregion STARI KOD
    }

    @Override
    public void promijeniListu(String kod) {
        Bundle arguments = new Bundle();
        arguments.putString("kod", kod);
        FragmentLista fl = new FragmentLista();
        fl.setArguments(arguments);

        getFragmentManager().beginTransaction().replace(R.id.frame_lista, fl).addToBackStack(null).commit();
    }

    @Override
    public void prikaziDetalje(int pozicija) {
        Bundle argumenti = new Bundle();
        argumenti.putParcelable("glumac", kontejner.glumci.get(pozicija));
        FragmentDetaljiGlumac fdg = new FragmentDetaljiGlumac();
        fdg.setArguments(argumenti);
        if(desni == null)
            getFragmentManager().beginTransaction().replace(R.id.frame_lista, fdg).addToBackStack(null).commit();
        else
            getFragmentManager().beginTransaction().replace(R.id.frame_wide_desno, fdg).commit();
    }

    @Override
    public void promijeniFragmenteWide(String kod) {
        if(kod == "ostalo") {
            FragmentManager fm = getFragmentManager();
            FragmentLista fl = new FragmentLista();
            Bundle arguments = new Bundle();
            arguments.putString("kod", "reziseri");
            fl.setArguments(arguments);
            fm.beginTransaction().replace(R.id.frame_lista, fl).commit();

            FragmentLista fl2 = new FragmentLista();
            Bundle arguments2 = new Bundle();
            arguments2.putString("kod", "zanrovi");
            fl2.setArguments(arguments2);
            fm.beginTransaction().replace(R.id.frame_wide_desno, fl2).commit();
        }
        else if(kod == "filmovi"){
            FragmentManager fm = getFragmentManager();
            FragmentWideDugmad fwd = (FragmentWideDugmad) fm.findFragmentById(R.id.frame_wide_dugmad);
            fm.beginTransaction().hide(fwd).commit();

            FragmentFilm ff = new FragmentFilm();

            fm.beginTransaction().replace(R.id.frame_lista, ff).commit();
        }
        else {
            FragmentManager fm = getFragmentManager();
            FragmentLista fl = new FragmentLista();
            Bundle arguments = new Bundle();
            arguments.putString("kod", "glumci");
            fl.setArguments(arguments);
            fm.beginTransaction().replace(R.id.frame_lista, fl).commit();

            FragmentDetaljiGlumac fdg = new FragmentDetaljiGlumac();
            Bundle argumenti = new Bundle();
            argumenti.putParcelable("glumac", kontejner.glumci.get(0));
            fdg.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.frame_wide_desno, fdg).commit();
        }

    }

    @Override
    public void spasiUBazu(Glumac g) {
        ArrayList<Glumac> temp = (ArrayList) Glumac.find(Glumac.class, "sifra = ?", Integer.toString(g.getSifra()));
        if(temp.size() == 0) {
            g.save();

            for (Zanr z : kontejner.zanrovi)
                if(!postojiLiZanrUBazi(z))
                    z.save();

            for (Reziser r : kontejner.reziseri)
                if(!postojiLiReziserUBazi(r))
                    r.save();
        }
    }

    private boolean postojiLiZanrUBazi(Zanr zanr){
        List<Zanr> zanroviIzBaze = Zanr.listAll(Zanr.class);
        for(Zanr z : zanroviIzBaze)
            if(z.getNaziv().equals(zanr.getNaziv()))
                return true;

        return false;
    }

    private boolean postojiLiReziserUBazi(Reziser reziser){
        List<Reziser> reziseriIzBaze = Reziser.listAll(Reziser.class);
        for(Reziser r : reziseriIzBaze)
            if(r.getNaziv().equals(reziser.getNaziv()))
                return true;

        return false;
    }

    @Override
    public void brisiIzBaze(Glumac g) {
        List<Glumac> temp = Glumac.find(Glumac.class, "sifra = ?", Integer.toString(g.getSifra()));
        if(temp.size() != 0) {
            long id = temp.get(0).getId();
            temp.get(0).delete();
            List<Reziser> tempR = Reziser.find(Reziser.class, "glumac = ?", Long.toString(id));
            for(Reziser r : tempR)
                r.delete();

            List<Zanr> tempZ = Zanr.find(Zanr.class, "glumac = ?", Long.toString(id));
            for(Zanr z : tempZ)
                z.delete();
        }
    }

    @Override
    public void promijeniNaFilmove() {
        FragmentManager fm = getFragmentManager();
        FragmentLista fl = (FragmentLista) fm.findFragmentById(R.id.frame_lista);
        FragmentDugmad fd = (FragmentDugmad)fm.findFragmentById(R.id.frame_dugmad);

        FragmentFilm ff = new FragmentFilm();

        fm.beginTransaction().hide(fd).commit();
        fm.beginTransaction().replace(R.id.frame_lista, ff).commit();
    }

    @Override
    public void prikaziDetaljeFilm(String naziv) {
        desni = (FrameLayout)findViewById(R.id.frame_wide_desno);

        if(desni != null) {
            FragmentManager fm = getFragmentManager();
            FragmentDetaljiFilm fdf = new FragmentDetaljiFilm();
            Bundle argumenti = new Bundle();
            argumenti.putString("naziv", naziv);
            fdf.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.frame_wide_desno, fdf).commit();
        }
        else {
            FragmentManager fm = getFragmentManager();
            FragmentDetaljiFilm fdf = new FragmentDetaljiFilm();
            Bundle argumenti = new Bundle();
            argumenti.putString("naziv", naziv);
            fdf.setArguments(argumenti);
            fm.beginTransaction().replace(R.id.frame_lista, fdf).commit();
        }
    }
}
