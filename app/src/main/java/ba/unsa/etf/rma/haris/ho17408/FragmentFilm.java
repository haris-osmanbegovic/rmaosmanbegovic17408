package ba.unsa.etf.rma.haris.ho17408;

import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class FragmentFilm extends Fragment implements MojResultReceiver.Receiver {
    static Context myContext;
    View za_vratiti;
    MojResultReceiver resiver;
    Film activity;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, Bundle savedInstanceState) {
        za_vratiti = inflater.inflate(R.layout.fragment_filmovi, container, false);
        myContext = getContext();

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(dajKontekst(), android.R.layout.simple_list_item_1, kontejner.filmovi);
        final ListView lista = (ListView) za_vratiti.findViewById(R.id.listaFilmova);
        lista.setAdapter(adapter);
        Button pretraga = (Button) za_vratiti.findViewById(R.id.traziFilm);
        pretraga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText uneseniTekst = (EditText) za_vratiti.findViewById(R.id.pretragaFilm);
                resiver = new MojResultReceiver(new Handler());
                resiver.setReceiver(FragmentFilm.this);
                Intent intent = new Intent(Intent.ACTION_SYNC, null, getContext(), PretragaTheMovieDB.class);
                intent.putExtra("res", resiver);
                intent.putExtra("komanda", "dajfilmove");
                intent.putExtra("query", uneseniTekst.getText().toString());

                String temp = uneseniTekst.getText().toString();

                getActivity().startService(intent);
            }
        });

        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                try{
                    activity = (Film) getActivity();
                    activity.prikaziDetaljeFilm(lista.getItemAtPosition(position).toString());
                }
                catch(Exception e){
                    //nije implementirano
                }
            }
        });

        return za_vratiti;
    }

    public Context dajKontekst(){
        return myContext;
    }

    @Override
    public void onReceiveResult(int resultCode, Bundle ResultData) {
        switch(resultCode){
            case 0:
                Toast.makeText(dajKontekst(), "Dohvacanje filmova u toku", Toast.LENGTH_LONG).show();
                break;
            case 1:
                ListView lista = (ListView) za_vratiti.findViewById(R.id.listaFilmova);
                ArrayAdapter<String> aa = (ArrayAdapter) lista.getAdapter();
                aa.notifyDataSetChanged();
                lista.requestLayout();
                break;
            case 2:
                Toast.makeText(dajKontekst(), "Greska", Toast.LENGTH_LONG).show();
        }
    }

    public interface Film{
        public void prikaziDetaljeFilm(String naziv);
    }
}
